package com.lumenagile.devhub.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;

import com.lumenagile.devhub.R;
import com.lumenagile.devhub.adapters.CustomAdapter;

import java.util.LinkedList;
import java.util.List;

public class ListActivity extends AppCompatActivity {

    ListView listView;
    CustomAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);



        List<String> elements = new LinkedList<String>();
        elements.add("Alejando");
        elements.add("Luis");
        elements.add("Lionel");

        listView = (ListView)findViewById(R.id.stringList);
        adapter = new CustomAdapter(this, elements, R.layout.item_listview);
        listView.setAdapter(adapter);
    }
}
