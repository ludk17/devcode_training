package com.lumenagile.devhub.adapters;


import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.lumenagile.devhub.R;

import java.util.List;

public class CustomAdapter extends BaseAdapter {

    private Activity activity;
    private List<String> elements;
    private int layout;

    public CustomAdapter(Activity activity, List<String> elements, int layout) {
        this.activity = activity;
        this.elements = elements;
        this.layout = layout;
    }

    @Override
    public int getCount() {
        return elements.size();
    }

    @Override
    public String getItem(int position) {
        return elements.get(position);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {

        LayoutInflater inflater = activity.getLayoutInflater();
        view = inflater.inflate(layout, null);

        TextView tvName = (TextView) view.findViewById(R.id.tvName);
        tvName.setText(getItem(i));


        return view;
    }
}
